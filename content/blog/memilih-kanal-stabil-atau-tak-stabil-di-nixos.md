---
layout: blog
draft: false
date: 2023-12-24T21:06:03+07:00
title: "Memilih Kanal Stabil atau Tak Stabil di NixOS?"
description: "Ulasan menggunakan dan memilih repo di kanal stabil (stable) atau tak stabil (unstable) di NixOS linux."
image: "images/blog/memilih-kanal-stabil-atau-tak-stabil-di-nixos.png"
type: "regular" # featured/regular
categories: # max 2
- nixos
- channel
---

NixOS memiliki beberapa kanal resmi dengan dua kategori yaitu kanal stabil (_stable_) atau tak stabil (_unstable_), dan kanal besar (_large_) atau kecil (_small_). Namun penulis hanya menjelaskan kanal stabil dan tak stabil dalam tulisan yang singkat ini.

#### Kanal stabil

Kanal repo stabil atau _stable_ merupakan kanal yang berisi paket dan dependensi yang telah menerima pembenahan dan kerentanan keamanan. Tidak menerima banyak pembaruan jika rilis awal. Kanal ini biasanya rilis setiap enam bulan sekali. Repo stabil menggunakan penomoran versi seperti `23.05`, `23.11`, `24.05`, dst. Dilihat dari penomoran maka perilisan stabil di bulan ke-5 (Mei) dan ke-11 (November), terkadang juga tidak menentu paling lambat awal bulan selanjutnya tergantung waktu senggang para pengembang NixOS.

Jika Anda menggunakan NixOS coba jalankan perintah ini untuk mengetahui jenis kanal aman yang digunakan.
```sh
sudo nix-channel --list
```

Contoh keluaran.
```sh
nixos https://nixos.org/channels/nixos-24.05
```

{{< lineads_1 >}}

#### Kanal tak stabil

Sedangkan kanal repo tak stabil atau _unstable_ merupakan kanal yang mengikuti cabang kode sumber di repo [unstable] NixOS. Sehingga lebih cepat menerima perubahan jika ada pembaruan paket aplikasi beserta dependensinya. Jangka waktu pembaruan menuju kanal tak stabil relatif lebih cepat, setidaknya hanya beberapa minggu sudah masuk ke cabang tak stabil.

```sh
sudo nix-channel --list
```
```sh
nixos https://nixos.org/channels/nixos-unstable
```

#### Memilih kanal sesuai kebutuhan

Hal ini tergantung dari kebutuhan dan sifat pengguna NixOS. Jika sebelumnya pengguna Arch linux, maka sebaiknya menggunakan kanal tak stabil (unstable). Dan jika sebelumnya menggunakan distribusi linux poin rilis seperti debian, ubuntu dan fedora maka sebaiknya menggunakan kanal stabil (stable).

Mengapa penulis mengatakan berdasarkan sifat pengguna? pada dasarnya pengguna ada 2 tipe dalam memilih distribusi linux, yaitu poin rilis (point release) dan rilis bergulir (rolling release). Ada yang lebih suka menunggu pembaruan, ada juga yang buru-buru tidak sabar menunggu fitur baru dari suatu aplikasi/pustaka sehingga lebih cocok dengan rilis bergulir.

Sama halnya dengan pemilihan kanal stabil dan tak stabil. Kanal stabil untuk pengguna yang tidak terlalu mementingkan pembaruan, sedangkan kanal tak stabil cocok bagi pengguna yang lebih mengedepankan pembaruan dan menyukai hal baru untuk uji coba atau sekedar menikmati fitur baru yang disediakan oleh suatu aplikasi/pustaka.

{{< lineads_2 >}}

#### Kesimpulan

Kanal mana yang lebih baik?

Jawabannya adalah kanal yang dipahami oleh penggunanya, maka itulah kanal terbaik. Setidaknya memahami kanal stabil/tak stabil dan mengerti ciri-cirinya pengguna semestinya sudah mengerti kanal mana yang harus dipilih.

Kanal yang sesuai dengan diri dan sifat penulis yaitu kanal tak stabil (_unstable_). Alasannya antara lain:

1. Lebih menyukai dan mencoba fitur baru dari suatu aplikasi/modul.
1. Jika ada kesalahan/galat/error dapat diatasi lebih cepat, hanya menunggu beberapa hari saja hingga dibenahi oleh pengembang [nixpkgs].
1. Kurang menyukai banyaknya perubahan [variabel pilihan], sehingga dengan menggunakan kanal tak stabil dapat dilakukan perubahan secara partial atau sedikit-demi sedikit agar tidak kebingungan.

Namun kekurangan dari kanal tak stabil lebih sedikit binary dari kanal stabil. Sehingga ada beberapa paket yang harus dikompil terlebih dahulu di mesin lokal sendiri.

Beberapa teman dari komunitas pengguna NixOS kebanyakan lebih suka menggunakan kanal tak stabil. Entah mereka sebelumnya berasal dari pengguna distro _rolling release_ atau memang sekedar ingin mencoba fitur baru dari suatu aplikasi.

Terakhir...

> Kita hidup cuma buat dua pilihan. Zona stabil atau zona tak stabil _(unstable)_. Silakan tentukan sendiri resikonya. _~@hervyqa_

Jika Anda mempunyai pertanyaan, saran, dan kritikan silahkan komentar dibawah
ini atau bisa menyapa penulis via [telegram @hervyqa](https://t.me/hervyqa).
Sekian, semoga tulisan ini bermanfaat untuk pembaca semuanya. Aamiin.

{{< lineads_3 >}}

[unstable]:https://github.com/NixOS/nixpkgs/tree/nixos-unstable
[nixpkgs]:https://github.com/NixOS/nixpkgs
[variabel pilihan]:https://search.nixos.org/options?
