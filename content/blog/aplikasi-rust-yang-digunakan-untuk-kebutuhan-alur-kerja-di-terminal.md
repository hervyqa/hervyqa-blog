---
layout: blog
draft: false
date: 2024-02-24T18:39:53+07:00
title: "Aplikasi Rust yang Digunakan untuk Kebutuhan Alur Kerja di Terminal"
description: "Aplikasi rust sebagai penunjang kebutuhan di terminal mulai dari sistem monitor, manajemen sesi, teks editor dan lain-lain."
image: "images/blog/aplikasi-rust-yang-digunakan-untuk-kebutuhan-alur-kerja-di-terminal.png"
type: "regular" # featured/regular
categories: # max 2
- rust
- terminal
---

Dalam tulisan ini menjelaskan secara singkat beberapa aplikasi CLI
_(command line interface)_ yang dibangun dengan bahasa [pemrograman
rust](https://www.rust-lang.org) untuk memnudahkan alur-kerja dengan
terminal. Tujuannya sebagai alternatif aplikasi yang sudah ada tetapi
memiliki fitur yang lebih. Dan perlu diketahui bahwa rust merupakan bahasa
pemrograman yang bisa diandalkan untuk performa yang tinggi dan penggunaan
memori yang rendah.

Yuk langsung saja!

#### Dysk

Selain menggunakan `df` untuk mengetahui partisi yang aktif atau tidaknya,
bisa menggunakan [Dysk](https://dystroy.org/dysk) yang lebih intuitif dengan
adanya grafik bar dan ditampilkan secara rapi dengan tabel.

{{< cmd >}}
dysk
{{< /cmd >}}

```sh
┌───────────────────┬────┬─────┬────┬─────────┬────┬────┬───────────┐
│     filesystem    │type│disk │used│   use   │free│size│mount point│
├───────────────────┼────┼─────┼────┼─────────┼────┼────┼───────────┤
│/dev/sda2          │ext4│ HDD │116G│89% ███  │ 18G│117G│/          │
│/dev/sda1          │vfat│ HDD │ 57M│11% ▌    │478M│536M│/boot/efi  │
└───────────────────┴────┴─────┴────┴─────────┴────┴────┴───────────┘
```

Anda juga dapat mengubah jenis kolom dengan format lain, misalnya:

{{< cmd >}}
dysk -c dev+fs+mp
{{< /cmd >}}

Daftar kolom dapat dilihat melalui `dysk --list-cols`.

Tidak banyak fitur yang ditampilkan oleh `dysk` selain menampilkan
partisi. Hanya saja dysk memiliki fitur yang lebih banyak dari pada `df`.

#### Eza

[Eza](https://github.com/eza-community/eza) merupakan pengganti dari
`ls` untuk menampilkan direktori dan berkas. Secara fungsi sama dengan
`ls`, hanya saja exa memiliki fitur lebih yaitu tampilan warna yang
sudah aktif dan memiliki fitur tree built-in yaitu untuk menampilkan
direktori/berkas seperti berakar.  Sebagai contoh, masuk ke dalam
direktori [swayhome](https://github.com/hervyqa/swayhome).

{{< cmd >}}
eza -l
{{< /cmd >}}

```sh
Permissions Size User    Date Modified Git Name
drwxr-xr-x     - hervyqa 22 Feb 20:53   -- home
drwxr-xr-x     - hervyqa  4 Feb 00:28   -- images
drwxr-xr-x     - hervyqa  1 Feb 15:12   -- nixos
.rw-r--r--    53 hervyqa 31 Jan 06:16   -- configuration.nix
.rw-r--r--  1.1k hervyqa  2 Jan 11:52   -- LICENSE.md
.rw-r--r--   13k hervyqa 24 Feb 17:08   -- README.md
```

Fitur tree built-in. Parameter `-T` untuk menampilkan tree dan `-D` untuk
menampilkan direktori saja.

{{< cmd >}}
eza -lT -D
{{< /cmd >}}

```sh
Permissions Size User    Date Modified Git Name
drwxr-xr-x     - hervyqa 24 Feb 04:48   -- .
drwxr-xr-x     - hervyqa 22 Feb 20:53   -- ├── home
drwxr-xr-x     - hervyqa 22 Feb 21:19   -- │  ├── config
drwxr-xr-x     - hervyqa 22 Feb 20:53   -- │  │  ├── kvantum
drwxr-xr-x     - hervyqa 22 Feb 21:08   -- │  │  ├── qt5ct
drwxr-xr-x     - hervyqa 22 Feb 21:08   -- │  │  ├── qt6ct
drwxr-xr-x     - hervyqa 22 Feb 21:19   -- │  │  └── rstudio
drwxr-xr-x     - hervyqa 22 Feb 21:19   -- │  │     └── themes
drwxr-xr-x     - hervyqa  8 Feb 18:51   -- │  ├── fonts
drwxr-xr-x     - hervyqa 23 Feb 20:13   -- │  ├── packages
drwxr-xr-x     - hervyqa 11 Feb 20:12   -- │  ├── programs
drwxr-xr-x     - hervyqa 23 Feb 04:22   -- │  ├── services
drwxr-xr-x     - hervyqa 22 Feb 20:59   -- │  ├── themes
drwxr-xr-x     - hervyqa  3 Feb 11:15   -- │  ├── wayland
drwxr-xr-x     - hervyqa 15 Jan 04:15   -- │  └── xdg
drwxr-xr-x     - hervyqa  4 Feb 00:28   -- ├── images
drwxr-xr-x     - hervyqa 22 Feb 20:10   -- │  ├── assets
drwxr-xr-x     - hervyqa  4 Feb 19:28   -- │  ├── gallery
drwxr-xr-x     - hervyqa  7 Feb 07:35   -- │  └── wallpapers
drwxr-xr-x     - hervyqa  1 Feb 15:12   -- └── nixos
drwxr-xr-x     - hervyqa 31 Jan 06:12   --    ├── hardware
drwxr-xr-x     - hervyqa 23 Feb 05:05   --    ├── programs
drwxr-xr-x     - hervyqa 20 Feb 23:28   --    ├── services
drwxr-xr-x     - hervyqa 17 Jan 17:28   --    ├── system
drwxr-xr-x     - hervyqa  1 Feb 15:18   --    ├── themes
drwxr-xr-x     - hervyqa  5 Jan 08:45   --    └── virtual
```

{{< lineads_1 >}}

#### Bat

[Bat](https://github.com/sharkdp/bat) merupakan alternatif dari `cat`
yang digunakan untuk menampilkan plain teks. Sefungsi dengan cat, hanya
saja bat memiliki nomer baris dan ada dukungan warna setiap format ekstensi
berkas. Sebagai contoh, akan menampilkan dari berkas `home/wayland/sway.nix`

{{< cmd >}}
bat home/wayland/sway.nix
{{< /cmd >}}

```sh
───────┬────────────────────────────────────────
       │ File: home/wayland/sway.nix
───────┼────────────────────────────────────────
   1   │ {
   2   │   lib,
   3   │   pkgs,
   4   │   ...
   5   │ }:
   6   │ with lib;
   7   │ with pkgs;
   8   │ let
   9   │   name = "hervyqa";
  10   │   modifier = "Mod4";
  11   │
  12   │   # navigation
  13   │   left = "h";
  14   │   down = "j";
  15   │   up = "k";
  16   │   right = "l";
  17   │
  18   │   # colors
  19   │   foreground = "#C6D0F5";   # text
  20   │   background = "#303446";   # base
  21   │   regular0   = "#51576D";   # surface 1
  22   │   regular1   = "#E78284";   # red
  23   │   regular4   = "#8CAAEE";   # blue
  24   │
  25   │ in {
  26   │   home-manager = {
  27   │     users.${name} = {
  28   │       wayland = {
  29   │         windowManager = {
  30   │           sway = {
```

#### Bottom

[Bottom](https://github.com/ClementTsang/bottom)sebagai alternatif dari
[htop](https://htop.dev) sebagai monitor proses di sistem dari setiap aplikasi
yang berjalan di latar belakang. Bottom yang eksekusi dengan `btm` memiliki
fitur grafik yang memungkinkan penggunanya menjadi lebih mudah dalam melihat
setiap proses aplikasi. Sama halnya dengan `htop`, `btm` juga dapat menampilkan
proses cpu, gpu, memori, penyimpanan, jaringan, temperatur dan lain sebagainya.

{{< cmd >}}
btm
{{< /cmd >}}

{{< image src="images/blog/bottom-monitor-demo.png" alt="bottom monitor proccess" >}}

Jika ingin melihat prosesnya saja atau tampilah dasar mirip seperti htop,
dapat menjalankan:

{{< cmd >}}
btm -b
{{< /cmd >}}

{{< image src="images/blog/bottom-monitor-basic-demo.png" alt="bottom monitor proccess" >}}

#### Fish Shell

[Fish shell](https://fishshell.com) merupakan salah satu shell favorit
pengganti bash shell yang dibangun dengan bahasa pemrograman rust ini memiliki
fitur yang luar biasa. Salah satunya _autocomplete_ yang sudah _built-in_
tanpa dependensi.

Kelebihan fish shell antara lain:
* Mendukung _syntax highlighting_.
* Terdapat _Inline auto-suggestions_ berdasarkan dari _history_.
* Ketika menekan tab muncul auto komplit yang menggunakan data man-page.
* Dukungan _intuitive wildcard_.
* Konfigurasi secara _web-based_.

Sebagai contoh, ketika mengetik `git` lalu menekan tab, maka akan muncul
autokomplit yang memudahkan untuk memilih perintah selanjutnya.

{{< cmd >}}
git
{{< /cmd >}}

```sh
add                             (Add file contents to the staging area)
am                                       (Apply patches from a mailbox)
apply                                                   (Apply a patch)
archive                        (Create an archive of files from a tree)
bisect                (Use binary search to find what introduced a bug)
blame                     (Show what last modified each line of a file)
branch                               (List, create, or delete branches)
bundle                  (Create, unpack, and manipulate "bundle" files)
checkout                              (Checkout and switch to a branch)
cherry                     (Find commits yet to be applied to upstream)
cherry-pick                        (Reapply a commit on another branch)
clean                    (Remove untracked files from the working tree)
…and 58 more rows
```

Fish-shell memiliki fitur `fish_config` yang mana dapat mengkonfigurasi
secara interaktif melalui website.

{{< cmd >}}
fish_config
{{< /cmd >}}

{{< image src="images/blog/fish-config-di-website.png" alt="fish config website" >}}

{{< lineads_2 >}}

#### Zellij

Setiap kali membuka terminal, [Zellij](https://zellij.dev) adalah aplikasi
pertama yang harus dijalankan terlebih dahulu. Zellij memudahkan manajemen
jendela terminal dan dapat disesuaikan sesuai kebutuhan. Dari pada membuka
terminal baru, penulis lebih senang untuk membuka tab baru di zellij. Lebih
cepat dan mudah.

Jika Anda pernah menggunakan [tmux](https://github.com/tmux/tmux), maka
tidak ada salahnya untuk mencoba zellij. Menurut pendapat subjektif penulis,
zellij lebih sederhana dan mudah dari pada menggunakan tmux.

{{< image src="images/blog/zellij-demo-4-pane.png" alt="zellij dengan 4 pane" >}}

Gambar di atas zellij yang terdiri dari 4 pane dengan menjalankan helix
(teks editor), ncmpcpp (pemutar audio), cava (audio visualizer), dan bottom
(sistem monitor).

#### Helix

[Helix](https://helix-editor.com) merupakan terminal teks editor sebagai
alternatif dari vim, neovim, emacs dan kakoune. Helix bisa diandalkan
sebagai teks editor utama, karena banyak _built-in_ fitur dan konfigurasi
yang minimal atau bahkan bisa dikatakan hampir zero config.

Adapun fitur helix yaitu:

* __Seleksi banyak plihan secara bersama.__
  Pengeditan kode multi-kursor secara bersamaan sudah built-in di helix.
* __Integrasi dengan Tree-sitter.__
  Mengaktifkan _syntax highlighting_, kalkulasi indent dan navigasi kode.
* __Manipulasi kode.__
  Mudahnya
[navigasi](https://docs.helix-editor.com/usage.html#tree-sitter-textobject-based-navigation)
dan penyeleksian fungsi, kelas, komentar, dan sebagainya.
* __Dukungan _Language server_.__
  Dengan spesifik bahasa autokomplit, menuju definisi, dokumentasi, diagnosa,
dan fitur IDE tanpa konfigurasi tambahan.
* __Dibangun dengan bahasa pemrograman rust.__
  Performa yang tinggi dan lebih hemat daya baterai.
* __Builtin fitur.__
  Fitur pencarian _fuzzy_ untuk perncarian berkas, simbol, projek, tema,
_fugitive_, _surround_ dan masih banyak lagi.

Ya benar, secara bawaan helix sudah mampu digunakan untuk koding dasar tanpa
ada plugin tambahan.

Penulis menjadikan helix sebagai teks editor utama untuk saat ini, bahkan
artikel ini pun ditulis dengan helix. Alasannya karena kebutuhan untuk
menulis artikel atau koding sudah tercukupi dengan fitur bawaan dari helix.

{{< image src="images/blog/helix-teks-editor-demo.png" alt="helix teks editor demo" >}}

Jika sebelumnya Anda pernah menggunakan vim, neovim, emacs atau teks editor
terminal lainnya. Serius, Helix patut dicoba.

#### Akhir Kata

Sebenarnya masih banyak aplikasi rust yang perlu penulis bahas, mungkin dalam
artikel selanjutnya. Contohnya seperti `Gitui` dan `Espanso` sebagai _text
extend_. Hanya saja untuk saat ini tidak bisa didemokan sebab gitui belum
mendukung _commit_ dengan git sign, dan espanso masih ada kendala tidak
dapat berjalan di sesi wayland yang sedang dipakai oleh penulis.

Artikel ini hanya memuat aplikasi rust yang pernah atau sedang dipakai
saja, jika ditelusuri lebih lanjut banyak sekali aplikasi rust yang sangat
bermanfaat. Misalnya `Rustdesk` sebagai pengganti TeamViewer dan `Lapce` Editor
pengganti VSCode, tetapi belum bisa memberikan ulasan karena hanya sebatas
mencoba beberapa waktu.

Harapannya setelah Anda membaca tulisan ini memiliki rasa ingin tahu dengan
salah satu aplikasi tersebut. Terutama 3 aplikasi favorit penulis yaitu
zellij, helix dan fish-shell. Lebih baik lagi jika menyempatkan untuk mencoba
dan mempelajarinya.

Sekian dari penulis..

Jika Anda mempunyai pertanyaan, saran, dan kritikan silahkan ~~komentar dibawah
ini atau bisa~~ menyapa penulis via [telegram @hervyqa](https://t.me/hervyqa).
Sekian, semoga tulisan ini bermanfaat untuk pembaca semuanya.

{{< lineads_3 >}}
