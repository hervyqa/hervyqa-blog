---
layout: blog
draft: false
date: 2023-05-16T14:57:35+07:00
title: "Cara Memasang Zotero dan Plugin LibreOffice Di NixOS"
description: "Panduan singkat memasang Zotero sebagai aplikasi manajemen bibliografi yang terintegrasi dengan LibreOffice dan Firefox browser"
image: "images/blog/cara-memasang-zotero-dan-plugin-libreoffice-di-nixos.png"
type: "regular" # featured/regular
categories: # max 2
- zotero
- nixos
---

[Zotero](https://www.zotero.org/) merupakan salah satu perangkat lunak pengolah daftar bibliografi yang wajib dimiliki oleh pelajar, mahasiswa dan tenaga pendidik. Terutama digunakan untuk pembuatan karya tulis, makalah, jurnal, skripsi, tesis dan tugas akhir lainnya.

Ada beberapa aplikasi lain yang serupa dengan Zotero tetapi alasan memilih Zotero karena aplikasi tersebut open source yang berlisensi AGPL-3.0-only (GNU Affero General Public License v3.0 only) serta dibuat secara native. Anda dapat melihat [kode sumbernya di tautan ini](https://github.com/zotero/zotero).

Ada pula seperti [Jabref](https://www.jabref.org/) yang dibuat dengan bahasa pemrograman java dan [Mendeley](https://www.mendeley.com/) yang nonfree, tetapi tidak penulis sarankan mengingat ada [kontra akademik dengan Elsevier](http://thecostofknowledge.com/) sebagai developer Mendeley.

Baik, langsung saja bagaimana cara memasang Zotero di NixOS.

#### Memasang Zotero

Anda bisa memasukkan langsung ke dalam lingkungan sistem NixOS. Misalnya seperti ini.

{{< file "/etc/nixos/configuration.nix" >}}

```
  systemPackages = with pkgs; [
    zotero
  ];
```

Kemudian bangun ulang sistem NixOSnya hingga Zotero selesai terpasang.

{{< cmd >}}
sudo nixos-rebuild switch
{{< /cmd >}}

Zotero merupakan aplikasi cross platform sehingga Anda dapat memasangnya di operasi lain seperti Windows, MacOS, dan sistem operasi lainnya. Silakan merujuk halaman unduh di https://www.zotero.org/download

#### Memasang LibreOffice Plugin

Zotero memiliki plugin untuk libreoffice sehingga dapat diintegrasikan keduanya.

* Langkah pertama, buka **Zotero**.
* Buka menu **Edit** > **Preferences**.
* Pilih Tab **Cite** > **Word Processor** > Pilih **Reinstall LibreOffice Add-in**.

{{< image src="images/blog/zotero-preferences.png" alt="zotero preferences" >}}

* Muncul dialog, klik **Next** > Pilih **Manual Installation**.
* Maka akan muncul file manager yang menuju ekstensi libreoffice **Zotero_OpenOffice_Integration.oxt**.
* Klik dua kali berkas tersebut agar dibuka dengan LibreOffice.
* Muncul dialog peringatan untuk memasang plugin Zotero ke LibreOffice. Pilih **OK**.

{{< image src="images/blog/zotero-integration.png" alt="zotero integration libreoffice" >}}

* Jika tidak muncul **Extension manager**, tekan <kbd><kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>E</kbd></kbd> untuk membuka dialog ekstensi manager.
* Tunggu beberapa saat hingga plugin Zotero tampil di **Extension manager**.

{{< image src="images/blog/zotero-installed.png" alt="zotero plugin installed" >}}

* Pilih **Close** dan **Restart Now** untuk memuat ulang LibreOffice.

#### Memasang Browser Addons

Tujuan memasang add-ons agar Zotero mampu mengintegrasikan rujukan dari browser kemudian disimpan ke basis data Zotero.

* Di bagian menu Zotero pilih **Tools** > **Install Browser Connector**. Atau buka tautan ini [zotero.org/download/connectors](https://www.zotero.org/download/connectors).
* Tautan tersebut akan mendeteksi jenis plugin berdasarkan Browser yang digunakan. Kemudian klik **Install**. Selain Firefox, Anda dapat menggunakan browser lainnya.

{{< image src="images/blog/zotero-browser-connector.png" alt="zotero browser connector" >}}

{{< lineads_1 >}}

#### Mengatur Dock Zotero di Libreoffice

Setelah berhasil memasang plugin Zotero, buka kembali libreoffice writer. Biasanya ada dock Zotero di toolbar atas sebelah kiri. Agar tampak rapi dan tidak menggeser dock lain, pindahkan dock zotero ke sebelah sisi kiri. Caranya seperti di video di bawah ini.

* Klik kanan pada dock Zotero, hapus ceklis **Lock Toolbar Position**.
* Seret dock ke sebelah kiri.
* Klik kanan pada dock Zotero lagi, aktifkan ceklis **Lock Toolbar Position**.

{{< video zotero-libreoffice.webm >}}

#### Membuat Daftar Bibliografi

* Sebelum membuat daftar biblografi, agar lebih mudah untuk memanajemen setiap artikel jurnal. Lebih baik membuat daftar koleksi dahulu dengan cata mengklik **New collection**, beri nama **Data Science** misalnya.

{{< image src="images/blog/zotero-new-collections.png" alt="zotero new collections" >}}

* Cari referensi di penyedia jurnal atau bahan karya ilmiah. Sebagai contoh buka tautan jurnal ini: https://doi.org/10.1007/s41060-020-00240-2

* Tambahkan melalui plugin Zotero di browser. Klik ikon Zotero, pilih daftar koleksi **Data Science** sebelumnya.

{{< image src="images/blog/zotero-add-paper.png" alt="zotero add paper" >}}

* Mengatur format bibliografi. Klik **Set Document Preferences** yang berbentuk ikon Gear.
* Pilih format style yang diinginkan. Misalnya format **American Psychological Association (APA) 7th edition**. Klik **OK**.

{{< image src="images/blog/zotero-format-style.png" alt="zotero format style" >}}

* Tambahkan kutipan ke libreoffice. Klik **Add/Edit Citation**, Masukkan jenis artikel yang dicari di dalam dialog yang muncul. Lalu tekan **Enter**.

{{< image src="images/blog/zotero-add-citation.png" alt="zotero add citation" >}}

* Membuat daftar bibliografi. Arahkan kursor ke arah lokasi bibliografi. Klik **Add/Edit Bibliography**. Tunggu beberapa saat.

{{< image src="images/blog/zotero-add-bibliography.png" alt="zotero-add-bibliography" >}}

Daftar bibliografi akan diperbarui secara otomatis jika memasukkan artikel baru untuk dikutip.

Cara membuat bibliografi kurang lebih seperti di video berikut ini.

{{< video zotero-add-bibliography-full.webm >}}

{{< lineads_2 >}}

#### Sinkronisasi (Opsional)

Dengan sinkronisasi Anda mendapatkan akses untuk menyimpan data bibliografi. Biasanya akun gratis mendapatkan 300MB diawal pendaftaran.

* Buka tautan: [zotero.org/user/register](https://www.zotero.org/user/register)
* Isi data pribadi Anda.
* Setelah selesai membuat akun, buka menu **Edit** > **Preferences**.
* Pilih **Cite** > **Setting**. Isi username dan password yang telah dibuat sebelumnya kemudian klik **Set up Synching**. Tunggu beberapa saat hingga selesai terintegrasi.

Demikian cara memasang Zotero dan plugin di Libreoffice serta mengintegrasikannya dengan web browser.

Jika Anda mempunyai pertanyaan, saran, dan kritikan silahkan komentar
dibawah ini atau di akun [telegram @hervyqa](https://t.me/hervyqa).
Sekian, semoga tulisan ini bermanfaat untuk pembaca semuanya. Aamiin.
