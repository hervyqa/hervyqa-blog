---
layout: blog
draft: true
date: {{ .Date }}
title: "{{ replace .Name "-" " " | title }}"
description: "This is meta description"
image: "images/blog/{{ .Name }}.png"
type: "regular" # featured/regular
categories: # max 2
-
-
---

#### Judul

Paragraf

{{< lineads_1 >}}
{{< lineads_2 >}}
{{< lineads_3 >}}

##### Sub Judul

Paragraf

- <kbd>a</kbd> = Fungsi
- <kbd>Ctrl</kbd> + <kbd>a</kbd> = Fungsi
- <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>a</kbd> = Fungsi
- <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>a</kbd> = Fungsi

{{< file "HelloWorld.scala" >}}

```scala
object HelloWorld extends App {
  println("Hello, World!")
}
```

{{< cmd >}}
hx learn.py
{{< /cmd >}}

{{< image src="images/blog/group-telegram.png" alt="alternate title" >}}

Jika Anda mempunyai pertanyaan, saran, dan kritikan silahkan ~~komentar dibawah
ini atau bisa~~ menyapa penulis via [telegram @hervyqa](https://t.me/hervyqa).
Sekian, semoga tulisan ini bermanfaat untuk pembaca semuanya.
